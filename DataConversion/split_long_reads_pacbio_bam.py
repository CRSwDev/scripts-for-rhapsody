# Requires: biopython, pysam

# Example command:
    # python3 split_long_reads_pacbio_bam.py segmented.bam

# Note - this script outputs two uncompressed FASTQ files.  
# Compress them with gzip before running in the Rhapsody Sequence Analysis Pipeline
# e.g. gzip segmented-R1.fastq
#      gzip segmented-R2.fastq



from Bio import Align
from Bio.Seq import Seq
import gzip
import os
import pysam
import sys


# Alignment settings
aligner = Align.PairwiseAligner()
aligner.mode = 'local'
aligner.match_score = 1
aligner.mismatch_score = -2
aligner.gap_score = -2.5

primer_seq = 'CTACACGACGCTCTTCCGATCT'
maximum_read_length = 15000

bam_file_path = sys.argv[1]

# Get base file name and directory
dirname = os.path.dirname(bam_file_path)
basename = os.path.basename(bam_file_path)
base, ext = os.path.splitext(basename)

samfile = pysam.AlignmentFile(bam_file_path, "rb", check_sq=False)

r1_file_path = os.path.join(dirname, f'{base}-R1.fastq')
r2_file_path = os.path.join(dirname, f'{base}-R2.fastq')

print(f'Writing to:\n{r1_file_path}\nand\n{r2_file_path}')

numBamRecords = 0
longest_read_length = 0

def write_to_fastq(read_id, seq, qual, alignments, r1write, r2write):

    target_align_end = alignments[0].coordinates[0][-1]
    cell_label_seq = seq[target_align_end:target_align_end+46+25]
    cell_label_qual = qual[target_align_end:target_align_end+46+25]

    if len(cell_label_seq) < 43:
        return

    r2seq = Seq(seq[target_align_end+46+25:]).reverse_complement()
    r2qual = qual[target_align_end+46+25:][::-1]

    if len(r2seq) < 40:
        return
    
    if len(r2seq) != len(r2qual):
        r2qual = 'I' * len(r2seq)
        print(f'non-equal lengths: {read_id}: {len(r2seq)} {len(r2qual)}')

    # Trim reads to a maximum length 
    if len(r2seq) > maximum_read_length:
        print(f'trimmed {read_id}, length: {len(r2seq)}')
        r2seq = r2seq[0:maximum_read_length]
        r2qual = r2qual[0:maximum_read_length]

    r1write.write(f'@{read_id} /1\n{cell_label_seq}\n+\n{cell_label_qual}\n')
    r2write.write(f'@{read_id} /2\n{r2seq}\n+\n{r2qual}\n')




with open(r1_file_path, 'w') as r1write, \
     open(r2_file_path, 'w') as r2write:

    for read in samfile.fetch(until_eof=True):

        numBamRecords += 1
        if numBamRecords % 100000 == 0:
            print(f'Processed {numBamRecords} reads')

        seq = read.query_sequence

        # Check the first 30 bases for the primer seq
        alignments = aligner.align(seq[:30], primer_seq)

        if alignments.score >= 14:
            if len(seq) > longest_read_length:
                longest_read_length = len(seq)
            read_id = read.query_name.replace('/', ':')
            qual = pysam.qualities_to_qualitystring(read.query_qualities)
            write_to_fastq(read_id, seq, qual, alignments, r1write, r2write)

        else:
            # If we didn't find a good alignment on the positive strand, look for the primer on the reverse complement
            seqrc = Seq(seq).reverse_complement()
            alignments = aligner.align(seqrc[:30], primer_seq)
            
            if alignments.score >= 14:
                if len(seqrc) > longest_read_length:
                    longest_read_length = len(seqrc)
                read_id = read.query_name.replace('/', ':')
                qualr = pysam.qualities_to_qualitystring(read.query_qualities)[::-1]  # reverse the quality to match the reversed sequence
                write_to_fastq(read_id, seqrc, qualr, alignments, r1write, r2write)

    print(f'longest_read_length: {longest_read_length}')
    print('done')


