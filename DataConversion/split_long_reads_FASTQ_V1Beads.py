# Requires biopython
# Run like this:
    # python3 split_long_reads_Rhapsody-V1.py my-long-read-library.fastq.gz


import itertools
import gzip
from Bio import pairwise2
import os
import sys

def read_fastq_seqs(filepath):
    with gzip.open(filepath, 'rb') as fh:
        for seq_header, seq, qual_header, qual in itertools.zip_longest(*[fh] * 4):
            if any(line is None for line in (seq_header, seq, qual_header, qual)):
                raise Exception(
                    "Number of lines in FASTQ file must be multiple of four "
                    "(i.e., each record must be exactly four lines long).")
            # if not seq_header.startswith('@'):
            #     raise Exception("Invalid FASTQ sequence header: %r" % seq_header)
            # if qual_header != '+\n':
            #     raise Exception("Invalid FASTQ quality header: %r" % qual_header)
            # if qual == '\n':
            #     raise Exception("FASTQ record is missing quality scores.")

            yield seq_header.decode('utf-8').rstrip('\n'), seq.decode('ascii').rstrip('\n'), qual.decode('ascii').rstrip('\n')


nn = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'N': 'N'}
def rev_compl(seq):
    return "".join(nn[n] for n in reversed(seq))


fastq_file_path = sys.argv[1]

# Get base file name
dirname = os.path.dirname(fastq_file_path)
basename = os.path.basename(fastq_file_path)
base, ext = os.path.splitext(basename)

primer_seq = 'ACACGACGCTCTTCCGATCT'

r1_out_path = os.path.join(dirname, f'{base}-R1.fastq.gz')
r2_out_path = os.path.join(dirname, f'{base}-R2.fastq.gz')

# Open R1 and R2 fastq files for writing
with gzip.open(r1_out_path, 'wt') as r1_file, \
     gzip.open(r2_out_path, 'wt') as r2_file:

    for header, seq, qual in read_fastq_seqs(fastq_file_path):
        # Perform the local pairwise alignment
        alignments = pairwise2.align.localms(seq, primer_seq, 4, -4, -10, -1)
        best_alignment_forward = max(alignments, key=lambda x: x.score)

        best_alignment_revComp = None
        # Maximum score is 4 * primer length = 80
        if best_alignment_forward.score < 70:
            rev_comp_seq = rev_compl(seq)
            revComp_alignments = pairwise2.align.localms(rev_comp_seq, primer_seq, 4, -4, -10, -1)
            best_alignment_revComp = max(revComp_alignments, key=lambda x: x.score)
        
        best_alignment_toUse = best_alignment_forward
        isForward = True
        if not best_alignment_revComp is None:
            if best_alignment_revComp.score > best_alignment_forward.score:
                best_alignment_toUse = best_alignment_revComp
                isForward = False

        if best_alignment_toUse.score > 40:

            primer_alignment_end = int(best_alignment_toUse.end)
            r1EndIndex = primer_alignment_end + 65
            if isForward == True:
                r1seq = seq[primer_alignment_end : r1EndIndex]
                r1qual = qual[primer_alignment_end : r1EndIndex]

                r2seq = rev_compl(seq[r1EndIndex:])
                r2qual = qual[r1EndIndex:][::-1]
            else:
                r1seq = rev_comp_seq[primer_alignment_end : r1EndIndex]
                r1qual = qual[::-1][primer_alignment_end : r1EndIndex]

                r2seq = rev_compl(rev_comp_seq[r1EndIndex:])
                r2qual = qual[::-1][r1EndIndex:][::-1]
            
            r1_file.write(f'{header.split()[0]} /1\n{r1seq}\n+\n{r1qual}\n')
            r2_file.write(f'{header.split()[0]} /2\n{r2seq}\n+\n{r2qual}\n')

