#! /usr/bin/env bash
set -euo pipefail

# Read in a datatable MEX such as the kind produced by BD Rhapsody Sequence Analysis Pipeline Version 2.0
# Convert to v1 style CSV datatable.
# This script takes filename ending in "_MEX.zip" as its argument.

if test $# != 1 ; then
    echo "Number of arguments is not 1.
        Usage: bash ./convert_datatableMEX_to_CSV.sh <Input_MEX> 
          Input_MEX: a _MEX.zip file such as the kind produced by BD Rhapsody Sequence Analysis Pipeline Version 2.0.
        " 1>&2
    exit
fi

input="$1"
if test "${input: -8}" != "_MEX.zip" ; then
    echo "Input filename must end with '_MEX.zip'
        Usage: bash ./convert_datatableMEX_to_CSV.sh <Input_MEX> 
          Input_MEX: a _MEX.zip file such as the kind produced by BD Rhapsody Sequence Analysis Pipeline Version 2.0.
        " 1>&2
    exit
fi

unzip $input

gunzip barcodes.tsv.gz features.tsv.gz matrix.mtx.gz

csv_file=${input/%_MEX.zip/.csv}

# Add CSV header lines if they exist in MTX file
awk -v csv="$csv_file" '
    /^%#/ {gsub("%", ""); print >> csv}
    /^%[A-Za-z]/ {gsub("%", "## "); print >> csv}
    /^[0-9]/ {exit}
' matrix.mtx

# Add row to CSV with feature names as columns headers
awk -v csv="$csv_file" '
    BEGIN {printf "Cell_Index" >> csv}
    {printf ",%s", $1 >> csv}
    END {printf "\n" >> csv}
' features.tsv

# Add rows to CSV with cell index and count data for each cell
awk -v csv="$csv_file" '
    BEGIN {prev=""}
    {
        if ($0 ~ /^[0-9]/)
            if (prev ~ /^%/) {
                m=$1
                n=$2
                l=$3
            }
            else
                a[$2, $1] = $3
        prev=$0
    }
    END {
        for (i=1; i<=n; i++) {
            getline line < "barcodes.tsv"
            printf "%s", line >> csv
            for (j=1; j<=m; j++)
                if ((i, j) in a)
                    printf ",%s", a[i, j] >> csv
                else
                    printf ",0" >> csv
            printf "\n" >> csv
        }
    }
' matrix.mtx

rm barcodes.tsv features.tsv matrix.mtx
