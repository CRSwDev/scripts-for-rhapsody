import pandas as pd
import csv
# pip install primer3-py
import primer3 as p3
from argparse import ArgumentParser

# A script for calculating primer interactions
# input: csv files containing at least 2 columns: primer_id and sequence
# primer files from portal would work
# outputs: hetero_dimer.csv and homo_dimer.csv
# hetero_dimer.csv: calculate all possible primer's heterodimer data: Tm, dG, dH, dS
# Homo_dimer.csv: calculate homodimer properties and hairpin properties for each primer sequence provided
# usage: python3 primer_compatibility.py -f file1.csv file2.csv file3.csv...


parser = ArgumentParser('check primer interaction between a list of files', add_help=True)
parser.add_argument('-f', action='store', dest='raw_files', required=True, help='primer files (csv): primer_id, sequence; usage: -f file1.csv, file2.csv, file3.csv ...', nargs='+')
args = parser.parse_args()

print("Processing " + str(args.raw_files))


all_primer_dict = {}
for file in args.raw_files:
    df = pd.read_csv(file)
    for index, row in df.iterrows():
        try:
            all_primer_dict[row['primer_id']] = row['sequence']
        except KeyError:
            print("The primer csv is not in correct format; please have a row named primer_id and another row named sequence")



# calculate hertero dimers
all_primers_list = all_primer_dict.values()

with open("hetero_dimer.csv", "w") as csv_file:
    writer = csv.writer(csv_file, delimiter=',')
    header = ["primer_id_1", "sequence1", "primer_id_2", "sequence2", "Tm", "dg", "dh", "ds"]
    writer.writerow(header)
    for primer1 in all_primer_dict:
        seq1 = all_primer_dict[primer1]
        for primer2 in all_primer_dict:
            seq2 = all_primer_dict[primer2]
            result = p3.calcHeterodimer(seq1, seq2, dv_conc=1.5, dntp_conc=0.6)
            writer.writerow([primer1, seq1, primer2, seq2, result.tm, result.dg/1000, result.dh, result.ds])


# check basic primer qualities
quality_dict = {}
for primer_id in all_primer_dict:
    quality_dict[primer_id] = {}
    quality_dict[primer_id]['sequence'] = all_primer_dict[primer_id]
    quality_dict[primer_id]['Tm'] = p3.calcTm(quality_dict[primer_id]['sequence'], dv_conc=1.5, dntp_conc=0.6)
    quality_dict[primer_id]['Gc'] = (quality_dict[primer_id]['sequence'].count('G') + quality_dict[primer_id]['sequence'].count('C'))/len(quality_dict[primer_id]['sequence'])
    quality_dict[primer_id]['Hairpin'] = p3.calcHairpin(quality_dict[primer_id]['sequence'], dv_conc=1.5, dntp_conc=0.6).todict()
    quality_dict[primer_id]['Homodimer'] = p3.calcHomodimer(quality_dict[primer_id]['sequence'], dv_conc=1.5, dntp_conc=0.6).todict()

#print(quality_dict)

with open("homo_dimer.csv", "w") as csv_file:
    writer = csv.writer(csv_file, delimiter=',')
    header = ['primer', 'sequence', 'Tm', 'Gc', 'Hairpin_detected', 'Hairpin_tm', 'Hairpin_dg', 'Hairpin_dh', \
    'Hairpin_ds', 'Homodimer_detected', 'Homodimer_tm', 'Homodimer_dg', 'Homodimer_dh', 'Homodimer_ds']
    writer.writerow(header)
    for item in quality_dict:
        writer.writerow([item, quality_dict[item]['sequence'], quality_dict[item]['Tm'], quality_dict[item]['Gc'], \
            quality_dict[item]['Hairpin']['structure_found'], quality_dict[item]['Hairpin']['tm'], quality_dict[item]['Hairpin']['dg'],\
            quality_dict[item]['Hairpin']['dh'], quality_dict[item]['Hairpin']['ds'], quality_dict[item]['Homodimer']['structure_found'], \
            quality_dict[item]['Homodimer']['tm'], quality_dict[item]['Homodimer']['dg'],quality_dict[item]['Homodimer']['dh'], quality_dict[item]['Homodimer']['ds']])








