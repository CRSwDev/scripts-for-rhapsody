import sys
import argparse
import HTSeq



def validate_gtf(annotation, complete = False):
    """ Confirm that the reference GTF file has the expected gene, transcript, and exon features. """

    gff_file = HTSeq.GFF_Reader(annotation, end_included=True)
    # Keep a list of gene_ids found in actual gene feature lines.
    gene_featureline_set = set()
    # Use a dict to track if genes have at least one associated transcript feature.
    genes = {}
    # Use a dict to track if transcripts have at least one associated exon feature.
    transcripts = {}
    # Keep track of how often non-fatal errors are seen.
    gene_missing_gene_name_count = 0
    transcript_missing_gene_name_count = 0
    exon_missing_gene_name_count = 0
    transcript_before_gene_count = 0
    exon_before_transcript_count = 0
    gene_missing_gene_biotype_count = 0
    # Keep track of if fatal error is seen.
    fatal_error_seen = False
    gene_biotype_never_seen = True

    # Define functions internally so that they can use the parent 'complete' variable.
    def parse_gene_name(feature: HTSeq.GenomicFeature):
        try:
            gene_name = feature.attr['gene_name']
        except KeyError:
            #print(f"Warning: Reference file line {feature} does not have attribute 'gene_name'.")
            # Initiate gene_name as None.
            gene_name = None
        return(gene_name)

    def parse_gene_id(feature: HTSeq.GenomicFeature):
        try:
            gene_id = feature.attr['gene_id']
        except KeyError:
            if complete: 
                print(f"Error: Reference file line {feature} does not have attribute 'gene_id'.")
            else:
                sys.exit(f"Error: Reference file line {feature} does not have attribute 'gene_id'.")
        return(gene_id)

    def parse_transcript_id(feature: HTSeq.GenomicFeature):
        try: 
            transcript_id = feature.attr['transcript_id']
        except KeyError:
            if complete:
                print(f"Error: Reference file line {feature} does not have attribute 'transcript_id'.")
            else:
                sys.exit(f"Error: Reference file line {feature} does not have attribute 'transcript_id'.")
        return(transcript_id)

    def parse_gene_biotype(feature: HTSeq.GenomicFeature):
        try: 
            gene_biotype = feature.attr['gene_biotype']
        except KeyError:
            try: 
                gene_biotype = feature.attr['gene_type']
            except KeyError:
                if complete:
                    #print(f"Error: Reference file line {feature} does not have attribute 'gene_biotype' or 'gene_type'.")
                    gene_biotype = None
                else:
                    sys.exit(f"Error: Reference file line {feature} does not have attribute 'gene_biotype' or 'gene_type'.")
        return(gene_biotype)

    for feature in gff_file:
        # Keep track of genes we see.
        if feature.type == "gene":
            gene_id = parse_gene_id(feature)
            # Keep track of whether features have the 'gene_name' attribute.
            # Either all gene-related features should have a 'gene_name', or none should.
            if parse_gene_name(feature) is None:
                gene_missing_gene_name_count += 1
            elif gene_missing_gene_name_count != 0:
                if complete:
                    fatal_error_seen = True
                    print(f"Error: Feature {feature} does not have attribute 'gene_name', but previous features did.")
                else:
                    sys.exit(f"Error: Feature {feature} does not have attribute 'gene_name', but previous features did. Either all gene-related features should have a 'gene_name', or none should.")
            if gene_id not in gene_featureline_set:
                gene_featureline_set.add(gene_id)
            if gene_id not in genes:
                genes[gene_id] = False

        # Keep track of transcripts we see.
        if feature.type == "transcript":
            gene_id = parse_gene_id(feature)
            if parse_gene_name(feature) is None:
                transcript_missing_gene_name_count += 1
            elif transcript_missing_gene_name_count != 0:
                if complete:
                    fatal_error_seen = True
                    print(f"Error: Feature {feature} does not have attribute 'gene_name', but previous features did.")
                else:
                    sys.exit(f"Error: Feature {feature} does not have attribute 'gene_name', but previous features did. Either all gene-related features should have a 'gene_name', or none should.")
            # Mark that this gene has at least one transcript.
            if gene_id not in genes:
                #print(f"Warning: transcript feature {feature} found before its matching gene feature in reference file.")
                transcript_before_gene_count += 1
            genes[gene_id] = True
            transcript_id = parse_transcript_id(feature)
            if transcript_id not in transcripts:
                transcripts[transcript_id] = False

        # Confirm exon has necessary attributes.
        if feature.type == "exon":
            gene_id = parse_gene_id(feature)
            if parse_gene_name(feature) is None:
                exon_missing_gene_name_count += 1
            elif exon_missing_gene_name_count != 0:
                if complete:
                    fatal_error_seen = True
                    print(f"Error: Feature {feature} does not have attribute 'gene_name', but previous features did.")
                else:
                    sys.exit(f"Error: Feature {feature} does not have attribute 'gene_name', but previous features did. Either all gene-related features should have a 'gene_name', or none should.")
            if gene_id not in genes:
                # Mark that this gene has been seen.
                genes[gene_id] = False
                #print(f"Warning: exon feature {feature} found before its matching gene feature in reference file.")
                
            transcript_id = parse_transcript_id(feature)
            if transcript_id not in transcripts:
                #print(f"Warning: exon feature {feature} found before its matching transcript feature in reference file.")
                exon_before_transcript_count += 1
            # Mark that this transcript has at least one exon.
            transcripts[transcript_id] = True

        # Check for presence of gene_biotype or gene_type attribute.
        if (parse_gene_biotype(feature) is None):
            gene_missing_gene_biotype_count += 1
        else:
            gene_biotype_never_seen = False

    # Text output on any anomalies, possibly with fatal error.
    if gene_missing_gene_name_count != 0:
        print(f"Warning: {gene_missing_gene_name_count} gene feature(s) are missing the 'gene_name' attribute.")
    if transcript_missing_gene_name_count != 0:
        print(f"Warning: {transcript_missing_gene_name_count} transcript feature(s) are missing the 'gene_name' attribute.")
    if exon_missing_gene_name_count != 0:
        print(f"Warning: {exon_missing_gene_name_count} exon feature(s) are missing the 'gene_name' attribute.")
    if gene_missing_gene_name_count != 0 or transcript_missing_gene_name_count != 0 or exon_missing_gene_name_count != 0:
        print("'gene_name' attribute is often more human-readable than 'gene_id'; however, either all gene-related features should have a 'gene_name', or none should. ")
    if transcript_before_gene_count != 0:
        print(f"Warning: At least {transcript_before_gene_count} transcript feature(s) were present in the reference before their matching gene features.")
    if exon_before_transcript_count != 0:
        print(f"Warning: At least {exon_before_transcript_count} exon feature(s) were present in the reference before their matching transcript features.")
    if gene_missing_gene_biotype_count != 0:
        print(f"Warning: {gene_missing_gene_biotype_count} feature(s) do not have a 'gene_biotype' or 'gene_type' attribute. Be consistent in using either 'gene_type' or 'gene_biotype', and make sure it is present on every feature you want used in the Rhapsody reference.")

    # Confirm that all gene_ids seen had a gene feature line.
    if not set(genes.keys()) <= gene_featureline_set:
        genes_missing_features = set(genes.keys()) - gene_featureline_set
        if complete:
            fatal_error_seen = True
            print(f"Error: Gene(s) {genes_missing_features} do not have gene feature lines in reference file.")
        elif len(genes_missing_features) > 10:
            sys.exit(f"Error: {len(genes_missing_features)} genes do not have gene feature lines in reference file.")
        else:
            sys.exit(f"Error: Gene(s) {genes_missing_features} do not have gene feature lines in reference file.")

    # Confirm that all seen genes had at least one matching transcript feature.
    if not all(genes.values()):
        fatal_error_seen = True
        gene_list = list(genes.keys())
        gene_has_transcript = list(genes.values())
        missing_transcript_indices = [i for i, x in enumerate(gene_has_transcript) if not x]
        genes_missing_transcripts = [gene_list[i] for i in missing_transcript_indices]
        if complete:
            print(f"Error: Gene(s) {genes_missing_transcripts} do not have associated transcript features in reference file.")
        elif len(genes_missing_transcripts) > 10:
            sys.exit(f"Error: {len(genes_missing_transcripts)} genes do not have associated transcript features in reference file.")
        else:
            sys.exit(f"Error: Gene(s) {genes_missing_transcripts} do not have associated transcript features in reference file.")
    # Confirm that all seen transcripts had at least one matching exon feature.
    if not all(transcripts.values()):
        fatal_error_seen = True
        transcript_list = list(transcripts.keys())
        transcript_has_exon = list(transcripts.values())
        missing_exon_indices = [i for i, x in enumerate(transcript_has_exon) if not x]
        transcripts_missing_exon = [transcript_list[i] for i in missing_exon_indices]
        if complete: 
            print(f"Error: Transcript(s) {transcripts_missing_exon} do not have associated exon features in reference file.")
        elif len(transcripts_missing_exon) > 10:
            sys.exit(f"Error: {len(transcripts_missing_exon)} transcripts do not have associated exon features in reference file.")
        else:
            sys.exit(f"Error: Transcript(s) {transcripts_missing_exon} do not have associated exon features in reference file.")
    
    if fatal_error_seen or gene_biotype_never_seen:
        sys.exit(f"Reference file has one or more errors that need to be corrected.")
    else:
        print("Reference file may be a valid GTF, but review any warning messages.")


def main():
    """ Main method to check that a GTF file is suitable for Rhapsody pipeline. """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--complete',
        action = 'store_true',
        help='If this option is not set, the script will exit after the first error it finds.'
    )
    parser.add_argument(
        'gtf_file',
        type=str,
        help='A GTF file with reference information for the Rhapsody pipeline.'
    )
    args = parser.parse_args()
    validate_gtf(args.gtf_file, complete = args.complete)
    return 0


if __name__ == '__main__':
    main()