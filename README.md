# Scripts for Rhapsody


### Data Conversion

Convert between MEX/MTX (Seq pipeline v2) and CSV (Seq pipeline v1) datatables


### Downstream Analysis

Examples of how to use popular single-cell analysis packages with Rhapsody data.  Seurat, ScanPy, and more


### Misc
 - Rhapsody cell label details and associated python functions
 - Check targeted panel primers for compatibility


### Seq Pipeline Input
Validate inputs for the Sequence Analysis pipeline

